# CI&CD Pipeline With SonarQube

### First Up jenkins and SonarQube

![Logo](https://i.ibb.co/617MW5p/Capture.png)
-
![Logo](https://i.ibb.co/vcdtr83/Capture.png)

### install SonarQube Scanner plugin in jenkins

- Go to Manage Jnekins > Manage Plugin > install SonarQube Scanner Plugin

![Logo](https://i.ibb.co/2h5F0ZX/ff.jpg)

### config SonarQube Environment in Jenkins

-  Go to Manage Jenkins > Configure System > Scroll down and find SonarQube Server 
-  fill SonarQube Name & URL and credential (pass token from SonarQube)

![Logo](https://i.ibb.co/CbQ761X/Capture.png)

### Config Webhook in SonarQube Server (for return quality gate status to jenkins)

-  Go to Administration > Configuration > Webhook > create

![Logo](https://i.ibb.co/VVQ5V3b/Capture.png)

-  fill name of webhook
-  pass url jenkins + '/sonarqube-webhook'

![Logo](https://i.ibb.co/17LKhtf/Capture.png)

### create jenkins pipeline job 

![Logo](https://i.ibb.co/CbY3k0v/Capture.png)

- then tick enable github 

![Logo](https://i.ibb.co/cvPK9bP/Capture.png)

- Copy pipeline Below

```bash
pipeline {
    agent any
    // declare varaible
    environment {
        chatID = "767718288"
        botToken =  "bot5371006076:AAFtkwih1-l5lmlBjmg2V1hCPyPGFkqI55Q"
        linkGit = "https://gitlab.com/rathjr/sonarqube-testing.git"
    }
    stages {
         stage("notification") {
            steps {
                // curl telegram api for post messeage
                sh "curl https://api.telegram.org/${env.botToken}/sendMessage?chat_id=${env.chatID} -d text='----jenkins trigger----' && curl https://api.telegram.org/${env.botToken}/sendMessage?chat_id=${env.chatID} -d text='---> check your job console here : https://jenkins.malinka.dev/job/sonarqubewithjenkins/${currentBuild.number}/console'"
            }
        }
        stage("checkout project") {
            steps {
                // clone project
                 git url: "${env.linkGit}" , credentialsId: "microservice"
            }
        }
    stage("SonarQube Analysis") {
	    steps{
	    //  injenct sonarqube environment with sonarqube property
        withSonarQubeEnv('SonarQube') {
        //  Use Sonar Scanner (CLI) with property below
        //  project key it must uniqe \
        //  java.binaries is paths to directories containing the compiled bytecode files \
          sh """/var/lib/jenkins/tools/hudson.plugins.sonar.SonarRunnerInstallation/SonarQube/bin/sonar-scanner \
         -D sonar.projectKey=apd_demo \
         -D sonar.java.binaries=src \
            """
        }
}
  }
   stage("Quality gate") {
	steps{
	   // response quality gate if status failed exit pipeline
                waitForQualityGate abortPipeline: true
        }
}
        stage("build project") {
            steps {
                // build project
                withGradle {
                    sh 'sudo chmod +x gradlew && ./gradlew build'
                }
                sh 'echo "project build successfully..."'
            }
        }
       stage("deploy uat") {
            steps {
                // build docker images,push,run production container(port 19001)
                sh '''
            fun_Dockerfile(){
                    echo "FROM adoptopenjdk/openjdk11
                    RUN mkdir -p workspace
                    COPY build/libs/other-0.0.1-SNAPSHOT.jar /workspace
                    EXPOSE 8080"
                    echo 'ENTRYPOINT ["java","-jar","/workspace/other-0.0.1-SNAPSHOT.jar"]'
 }
            fun_Dockerfile > Dockerfile
                    echo $(docker build -t rathjr/sonarqubeuatuat .) > imagesLog_uat.txt
                    VR=$(grep 'Step 4'  imagesLog_uat.txt)
            if [ "$VR" == "" ]
            then
                    echo "images build failed"
            else
                    echo "build imaegs success"
                    docker login -u rathjr -p Rathkhmer2021
                    docker push rathjr/sonarqubeuatuat
                    docker rm -f sonarqubeuatuat
                    docker run -d --name sonarqubeuatuat -p 19000:8080 rathjr/sonarqubeuatuat
                    echo "done deploy uat"
            fi
          '''
            }
        }
    }
}
```
- save and build
### check your sonarqube report URL in stage Analysis

![Logo](https://i.ibb.co/YQMt2YZ/Capture.png)

### check details about your project vai link above (bug, Vulnerabilities, code smell)

![Logo](https://i.ibb.co/sjBGmwD/Capture.png)

### quality gate status that sonarqube webhook to jenkins 

![Logo](https://i.ibb.co/pR00Nbf/Capture.png)

### view all stage of pipeline

![Logo](https://i.ibb.co/RYTZ7q4/Capture.png)

### Config Github Webhook with jenkins

- go to project and click setting > webhook > add webhook

![Logo](https://i.ibb.co/dcvhVB8/Capture.png)

- pass url of jenkins server to payload url with /github-webhook/
- change conten type to application/json
- tick enable ssl verification
- tick just the push event

![Logo](https://i.ibb.co/0Yp6WXZ/Capture.png)
